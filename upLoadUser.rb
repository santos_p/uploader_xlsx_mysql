require 'roo'
require 'mysql2'

class UpLoadUser
  # Creation of variables. ADD here the connection config and XLSX file
  workbook = Roo::CSV.new('./users.csv', csv_options: {encoding: Encoding::UTF_8})
  host = '52.1.186.25'
  username = 'solvian'
  password = 'Wrr@3dDW'
  database = 'hml_smart_field_service_gbt'
  table = 'users'
  begin
    # Connect to DB
    con = Mysql2::Client.new(host: host, username: username, password: password, database: database, encoding: 'utf8')

    # Set the worksheet you want to work with as the default worksheet.  You could
    # also iterate over all the worksheets in the workbook.
    workbook.default_sheet = workbook.sheets[0]

    # Create a hash of the headers so we can access columns by name (assuming row
    # 1 contains the column headings).  This will also grab any data in hidden
    # columns.
    headers = {}
    workbook.row(1).each_with_index { |header, i| headers[header] = i }

    fields = []
    # Iterate over the rows using the `first_row` and `last_row` methods.  Skip
    # the header row in the range.
    ((workbook.first_row + 1)..workbook.last_row).each do |row|
      # Get the column data using the column heading.
      fields[1] = workbook.row(row)[headers['encrypted_password']]
      fields[2] = workbook.row(row)[headers['email']]
      fields[3] = workbook.row(row)[headers['username']]
      fields[4] = workbook.row(row)[headers['name']]

      # Checks to see if fields is nil and replace for empty
      fields.each { |field| field = '' if field.nil? }

      # Construct and send SQL command
      con.query("INSERT INTO users(
      encrypted_password,
      email,
      username,
      name,
      created_at,
      updated_at)
      VALUES(
      MD5('#{fields[1]}'),
      '#{fields[2]}',
      '#{fields[3]}',
      '#{fields[4]}',
      '#{Time.now.strftime("%Y/%m/%d %H:%M:%S")}',
      '#{Time.now.strftime("%Y/%m/%d %H:%M:%S")}')")

      puts "INSERT: #{fields[2]}"
    end
  rescue Mysql2::Error => e
    puts e.errno
    puts e.error
  ensure
    con.close if con
  end
end
