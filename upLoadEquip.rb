require 'roo'
require 'mysql2'

class UpLoadEquip
  # Creation of variables. ADD here the connection config and XLSX file
  workbook = Roo::CSV.new('./equip.csv', csv_options: {encoding: Encoding::UTF_8})
  host = '52.1.186.25'
  username = 'solvian'
  password = 'Wrr@3dDW'
  database = 'hml_smart_field_service_gbt'
  table = 'equipments'
  begin
    # Connect to DB
    con = Mysql2::Client.new(host: host, username: username, password: password, database: database, encoding: 'utf8')

    # Set the worksheet you want to work with as the default worksheet.  You could
    # also iterate over all the worksheets in the workbook.
    workbook.default_sheet = workbook.sheets[0]

    # Create a hash of the headers so we can access columns by name (assuming row
    # 1 contains the column headings).  This will also grab any data in hidden
    # columns.
    headers = {}
    workbook.row(1).each_with_index { |header, i| headers[header] = i }

    fields = []
    # Iterate over the rows using the `first_row` and `last_row` methods.  Skip
    # the header row in the range.
    ((workbook.first_row + 1)..workbook.last_row).each do |row|
      # Get the column data using the column heading.
      fields[1] = workbook.row(row)[headers['identification']]
      fields[2] = workbook.row(row)[headers['room_id']]
      fields[3] = workbook.row(row)[headers['place_service_id']]
      fields[4] = workbook.row(row)[headers['system_group_id']]
      fields[5] = workbook.row(row)[headers['active']]
      fields[6] = workbook.row(row)[headers['created_at']]
      fields[7] = workbook.row(row)[headers['updated_at']]
      fields[8] = workbook.row(row)[headers['inventory_number']]
      fields[9] = workbook.row(row)[headers['life_span']]
      fields[10] = workbook.row(row)[headers['mtbf_factory']]
      fields[11] = workbook.row(row)[headers['mtbf_measured']]
      fields[12] = workbook.row(row)[headers['average_downtime']]
      fields[13] = workbook.row(row)[headers['configuration_parameters']]
      fields[14] = workbook.row(row)[headers['registered_incident_management']]
      fields[15] = workbook.row(row)[headers['registered_problem_management']]
      fields[16] = workbook.row(row)[headers['history']]
      fields[17] = workbook.row(row)[headers['qr_code']]

      # Checks to see if fields is nil and replace for empty
      fields.each { |field| field = '' if field.nil? }

      # Construct and send SQL command
      con.query("INSERT INTO #{table}(
      identification,
      room_id,
      place_service_id,
      system_group_id,
      active,
      created_at,
      updated_at,
      inventory_number,
      life_span,
      mtbf_factory,
      mtbf_measured,
      average_downtime,
      configuration_parameters,
      registered_incident_management,
      registered_problem_management,
      history,
      qr_code)
      VALUES(
      '#{fields[1]}',
      #{fields[2]},
      #{fields[3]},
      #{fields[4]},
      #{fields[5]},
      'Time.now.strftime("%Y/%m/%d %H:%M:%S")',
      'Time.now.strftime("%Y/%m/%d %H:%M:%S")',
      '#{fields[8]}',
      '#{fields[9]}',
      '#{fields[10]}',
      '#{fields[11]}',
      '#{fields[12]}',
      '#{fields[13]}',
      '#{fields[14]}',
      '#{fields[15]}',
      '#{fields[16]}',
      '#{fields[17]}')")

      puts "INSERT: #{fields[1]} - #{fields[2]}"
    end
  rescue Mysql2::Error => e
    puts e.errno
    puts e.error
  ensure
    con.close if con
  end
end
